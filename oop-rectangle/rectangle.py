#!/usr/bin/python
"""
    Tyler Manifold
    CSCI230
    OOP Rectangle
"""

## Simple Quad class to represent a 4 sided shape
class Quad(object):
    def __init__(self, wid = 1, hei = 1):
        
        object.__init__(self)

        self.__width = wid
        self.__height = hei


    def getWidth(self):
        return self.__width
    
    def setWidth(self, w):
        self.__width = w

    def getHeight(self):
        return self.__height

    def setHeight(self, h):
        self.__height = h

    width = property(fget = getWidth, fset = setWidth)
    height = property(fget = getHeight, fset = setHeight)

## Extends the Quad class to create a Rectangle
##  with a calculated area and perimeter,
class Rectangle(Quad):
    
    ## Initialize the Rectangle by setting
    ##  the specified width and height
    def __init__(self, wid = 4, hei = 2):
        Quad.__init__(self, wid, hei)

        self.setHeight(wid)
        self.setHeight(hei)

    ## Print out the properties of the Rectangle
    def getStats(self):
        return ("Width: {}\nHeight: {}\nArea: {}\nPerimeter: {}".format(self.getWidth(), self.getHeight(), self.getArea(), self.getPerimeter()))

    def getPerimeter(self):
        return (2 * self.getWidth()) + (2 * self.getHeight())

    def getArea(self):
        return self.getWidth() * self.getHeight()

    area = property(fget = getArea)
    perimeter = property(fget = getPerimeter)

def main():
    print ("Rectangle a:")
    a = Rectangle(5, 7)
    print ("area:      {}".format(a.area))
    print ("perimeter: {}".format(a.perimeter))
                        
    print ("")
    print ("Rectangle b:")
    b = Rectangle()
    b.width = 10
    b.height = 20
    print (b.getStats())

if __name__ == "__main__":
    main()
