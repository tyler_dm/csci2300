#!/usr/bin/python
"""
	Tyler Manifold
	CSCI230
	OOP Rectangle blackbelt challenge
	Implement the OOP Rectangle class
	 in a graphical environment using
	 TK
"""

from tkinter import *
import random

class GUIRect(Tk):	
	def __init__(self):

		random.seed()

		Tk.__init__(self)

		self.scrWidth = Scale()
		self.scrHeight = Scale()
		self.height = DoubleVar()
		self.width = DoubleVar()

		self.MAX_HEIGHT = 100
		self.MAX_WIDTH  = 100

		self.title("GUI Rectangle")
		
		self.addLabels()
		self.addDimensionSliders()

		self.width.set(random.randint(0, self.MAX_WIDTH))
		self.height.set(random.randint(0, self.MAX_HEIGHT))

		self.canvas_height = 125
		self.canvas_width  = 150

		self.calcDimensions()

		self.rectangle_region = Canvas(self, bg = "#FFF", confine = True, width = self.canvas_width, height = self.canvas_height)
		self.rectangle_region.grid(row = 5, column = 0, columnspan = 2)

		self.DrawRect()

		self.mainloop()

	## Create and add all the labels
	def addLabels(self):
	
		# tuple of labels that do not store data
		label_names = ("Width:", "Height:", "Area:", "Perimeter:")
		
		# add the tile
		Label(self, text = "Rectangle").grid(row = 0, column = 0, columnspan = 2)
		
		# add all of the labels stored in label_names
		for r in range(len(label_names)):
			Label(self, text = label_names[r]).grid(row = (r + 1), column = 0)
			
		# add the labels that store data
		self.lblArea = Label(text = self.getArea()).grid(row = 3, column = 1, columnspan = 2, sticky = "ew")
		self.lblPeri = Label(text = self.getPerimeter()).grid(row = 4, column = 1, columnspan = 2, sticky = "ew")
		
	# add sliders for width and height
	def addDimensionSliders(self):
		
		self.width = DoubleVar()
		
		self.scrWidth = Scale(self, orient = HORIZONTAL, from_ = 1, to = 100, variable = self.width, command = self.makeRect)
		self.scrWidth.grid(row = 1, column = 1)
		
		self.height = DoubleVar()
		
		self.scrHeight = Scale(self, orient = HORIZONTAL, from_ = 1, to = 100, variable = self.height, command = self.makeRect)
		self.scrHeight.grid(row = 2, column = 1)
		
	def getArea(self):		
		return self.scrWidth.get() * self.scrHeight.get()
		
	def getPerimeter(self):
		return (2 * self.scrWidth.get()) + (2 * self.scrHeight.get())
		
	area = property(fget = getArea)
	perimeter = property(fget = getPerimeter)

	# Make the rectangle by calculating area and perimeter,
	#  then drawing the rectangle to the canvas
	def makeRect(self, pos):
		self.rectangle_region = Canvas(self, bg = "#FFF", confine = True, width = self.canvas_width, height = self.canvas_height)
		self.rectangle_region.grid(row = 5, column = 0, columnspan = 2)

		self.calcDimensions()
		self.DrawRect()
		
	# calculate the area and perimeter, then assign those values to
	#  their corresponding data label text
	def calcDimensions(self):
		self.lblArea = Label(text = self.getArea()).grid(row = 3, column = 1, columnspan = 2, sticky = "ew")
		self.lblPeri = Label(text = self.getPerimeter()).grid(row = 4, column = 1, columnspan = 2, sticky = "ew")

	# Using the values of the width and height scales,
	#  draw a rectangle to the canvas	
	def DrawRect(self):

		wid = self.width.get()
		height = self.height.get()

		center = self.getCanvasCenter()

		x1 = center[0] - (wid / 2)
		y1 = center[1] - (height / 2)

		x2 = center[0] + (wid / 2)
		y2 = center[1] + (height / 2)

		self.rect = self.rectangle_region.create_rectangle(x1, y1, x2, y2, fill = "#00F")

	# Calculate the centerpoint coordinates of the canvase
	#  and return a tuple containing the x,y values
	def getCanvasCenter(self):
		center = (self.canvas_width / 2, self.canvas_height / 2)
		
		return center

def main():

	app = GUIRect()

if __name__ == "__main__":
	main()
