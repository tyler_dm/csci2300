#!/usr/bin/python

"""
    Tyler Manifold
    CSCI230
    BMI Calculator
    Demonstrate your ability to create a graphic user interface in Python using the Tkinter framework.
     Build an application which prompts the user for his or her height and weight. Use this information
     to calculate the user's body mass index and provide feedback.
"""

from tkinter import *

class BMI(Tk):

    def __init__(self):
        Tk.__init__(self)

        
        self.__weight = StringVar()
        self.__heightFeet = StringVar()
        self.__heightInches = StringVar()

        Label(self, text = "BMI Calculator").grid(row = 0, columnspan = 2, sticky = "ew")

        self.addHeight()
        self.addWeight()
        self.addBMI()
        self.addStatus()
        self.addCalcButton()
        self.calcBMI()

        self.mainloop()

    # return height in inches
    def getHeight(self):
    
        ft = int(self.spnHeightFeet.get()) * 12

        inches = int(self.spnHeightInches.get()) + ft

        return inches

    def getWeight(self):
        return int(self.spnWeight.get())

    height = property(fget = getHeight)
    weight = property(fget = getWeight)

    def calcBMI(self):
        
        w = self.getWeight()
        h = self.getHeight()        
        
        print ("height: {}\nweight: {}".format(h, w))

        bmi = (w / (h ** 2)) * 703

        bmi_str = format(bmi, '.2f')

        self.lblBMI = Label(self, text = bmi_str, bg = "#FFF", relief = "sunken")
        self.lblBMI.grid(row = 4, column = 1, sticky = "ew", columnspan = 1)

        if bmi <= 18.5:
            status = "underweight"
        elif bmi >= 18.51 and bmi <= 24.9:
            status = "normal"
        elif bmi >= 25 and bmi <= 29.9:
            status = "overweight"
        elif bmi >= 30:
            status = "obese"
        else:
            status = "unknown"

        self.lblStatus = Label(self, text = status, bg = "#FFF", relief = "sunken")
        self.lblStatus.grid(row = 5, column = 1, sticky = "ew", columnspan = 1)


    def addHeight(self):
        Label(self, text = "Height").grid(row = 1, column = 0, sticky = "e")

        self.spnHeightFeet = Spinbox(self, from_ = 1, to = 7, textvariable = self.__heightFeet)
        self.spnHeightFeet.grid(row = 1, column = 1, sticky = "w")
       
        Label(self, text = "ft").grid(row = 1, column = 2, sticky = "w")

        self.spnHeightInches = Spinbox(self, from_ = 1, to = 11, textvariable = self.__heightInches)
        self.spnHeightInches.grid(row = 2, column = 1, sticky = "w")
        
        Label(self, text = "in").grid(row = 2, column = 2, sticky = "w")

    def addWeight(self):
        Label(self, text = "Weight").grid(row = 3, column = 0, sticky = "e")
        
        self.spnWeight = Spinbox(self, from_ = 1, to = 400, textvariable = self.__weight)
        self.spnWeight.grid(row = 3, column = 1, sticky = "w")
        
        Label(self, text = "lb").grid(row = 3, column = 2, sticky = "w")

    def addBMI(self):
        Label(self, text = "BMI").grid(row = 4, column = 0, sticky = "e")
        self.lblBMI = Label(self, text = "bmiText", bg = "#FFF", relief = "sunken")
        self.lblBMI.grid(row = 4, column = 1, sticky = "ew", columnspan = 1)

    def addStatus(self):
       Label(self, text = "Status").grid(row = 5, column = 0, sticky = "e")
       self.lblStatus = Label(self, text = "statusText", bg = "#FFF", relief = "sunken")
       self.lblStatus.grid(row = 5, column = 1, sticky = "ew", columnspan = 1)

    def addCalcButton(self):
        self.btnCalc = Button(self, text = "Calculate", command = self.calcBMI)
        self.btnCalc.grid(row = 6, column = 1, sticky = "we")

def main():
    app = BMI()

if __name__ == "__main__":
    main()
