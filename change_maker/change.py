
# Tyler Manifold
# CSCI230
# Change maker
#  Accepts a price and cash tendered as input,
#  then calculates the amount of the following
#  denominations to be returned:
#       20, 10, 5, 1, 0.25, 0.10, 0.05, 0.01

# initialize the register

register = 2000

# get a price from the user

price = input("Price:\n> $")

# get cash from the user

cash = input("Cash tendered:\n > $")

#price = 14.23
#cash = 20


price = float(price)
cash  = float(cash)
remaining = 0

difference = cash - price

print ("Difference: {}".format(format(difference, '.2f')))

# Determine amount of each denomination. 
#  If the user enters price p and cash c,
#  denomination d is the difference int-divided by the
#  denomination value.

# multiply everything by 100 to avoid floating point shenanigans
register *= 100
remaining *= 100
difference *= 100

# twenties
twenties = difference // 2000
remaining = difference % 2000
register -= (2000 * twenties)
#print("twenties: {}".format(twenties))
#print ("Remaining: {}\tRegister: {}".format(remaining, register))

# tens
tens = remaining // 1000
remaining = remaining % 1000
register -= (1000 * tens)
#print("\ntens: {}".format(tens))
#print ("Remaining: {}\tRegister: {}".format(remaining, register))

# fives
fives = remaining // 500
remaining = remaining % 500 
register -= (500 * fives)
#print("\nfives: {}".format(fives))
#print ("Remaining: {}\tRegister: {}".format(remaining, register))

# ones
ones = remaining // 100
remaining = remaining % 100
register -= ones
#print("\nones: {}".format(fives))
#print ("Remaining: {}\tRegister: {}".format(remaining, register))

# quarters
quarters = remaining // 25
remaining = remaining % 25
register -= (25 * quarters)
#print("\nquarters: {}".format(quarters))
#print ("Remaining: {}\tRegister: {}".format(remaining, register))

# dimes
dimes = remaining // 10
remaining = remaining % 10
register -= (10 * dimes)
#print("\ndimes: {}".format(dimes))
#print ("Remaining: {}\tRegister: {}".format(remaining, register))

# nickels
nickels = remaining // 5
remaining = remaining % 5
register -= (5 * nickels)
#print("\nnickels: {}".format(nickels))
#print ("Remaining: {}\tRegister: {}".format(remaining, register))

# pennies
pennies = remaining // 1
remaining = remaining % 1
register -= (.01 * pennies)
#print("\npennies: {}".format(pennies))
#print ("Remaining: {}\tRegister: {}".format(remaining, register))

twenties = int(twenties)
tens = int(tens)
fives = int(fives)
ones = int(ones)
quarters = int(quarters)
dimes = int(dimes)
nickels = int(nickels)
pennies = int(pennies)

register /= 100


#print ("Difference: ${}".format(difference))
print ("20\t10\t5\t1\t.25\t.10\t.05\t.01\treg")
print ("{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}".format(twenties, tens, fives, ones, quarters, dimes, nickels, pennies, format(register, '.2f')))


