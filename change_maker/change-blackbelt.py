#!/usr/bin/python
# Tyler Manifold
# CSCI230
# Change maker blackbelt
#  Vending Machine Simulator

import random
import os
import platform
import time

random.seed()

""" Generate a random integer from 1 to 10. If the random
	integer is 1, return True. This function is used to
	pseudo-randomly determine whether or not to reject 
	a bill submitted to the vending machine.
"""
def rejectBill(reject_mod):
	
	REJECT_MAX = 10

	if (reject_mod >= REJECT_MAX):
		reject_mod = 9

	if ((REJECT_MAX - reject_mod) < 1):
		reject_mod = 1

	r = random.randint(1, 11 - reject_mod)
	
	if (r ==  1):
		result = True
	else:
		result = False
	
	return result

def hum():
	print ("The vending machine hums ominously.")
	Wait()
	return

""" billEncounter defines the randomly folded bill process.
	 The repair_difficulty is represented by a random number
	 from 1-5, and is used to calculate chance of successfully
	 repairing a	folded bill.
"""
def billEncounter(bill_type):
		global wallet
		repair_skill = 4.0
		repair_difficulty = random.randint(1, 6)
		repair_difficulty = float(repair_difficulty)
		repair_chance = (repair_skill / (repair_difficulty + repair_skill)) * 100
		
		repair_chance = int(repair_chance)

		next_attempt = 0

		rejection_modifier = 0
		
		clearScreen()
		print ("Upon closer inspection, you notice a corner of the bill is slightly folded")
		repair = input ("\nAttempt to repair the bill? (Y/N)\nDifficulty: {}\nChance: {}%\n> ".format(repair_difficulty, repair_chance))
		
		repair = repair.upper()
		
		# user decides to attempt repair or not
		if (repair == 'Y'):
			#generate a random number from 10-100
			attempt = random.randint(10, 101) + next_attempt

			# if attempt is larger than repair_chance, the bill is repaired
			#  otherwise the bill is destroyed and the amount is subtracted
			#  from the wallet
			if (attempt > repair_chance):
				print ("You successfully repair the bill.")
			else :
				print("You accidentally tore the bill in two")
				wallet -= bill_type

			next_attempt = 0
			rejection_modifier = 0
		else:
			# return the bill to the wallet, but the next
			#  repair attempt will be more difficult, and
			#  bills will have an increased chance of being
			#  rejected
			rejection_modifier += 1
			next_attempt -= 1
			wallet += bill_type
		Wait()
		return rejection_modifier

# Simply prompt the user for input to continue
def Wait():
	input("Press ENTER to continue")
	return

# Calls the appropriate clear screen function depending one
#  what system is running the program
def clearScreen():
	if (platform.system() == 'Windows'):
		a = os.system("cls")
	else:
		b = os.system("clear")
	return

# loop through and print the elements of the parallel items[]
#  and price[] lists
def printMenu():
	clearScreen()
	global choice
	global price
	
	try:
		print("You approach the vending machine with ${} in your wallet.".format(formatFloat(wallet)))
		print ("What do you do?")

		# print a menu consisting of a numbered list of the available
		#  beverages and their coresponding prices
		for i in range(len(items)):
			print ("{}: {}${}".format(i + 1, items[i].ljust(12) , formatFloat(item_price[i]) ))
		choice = input("> ")
		
	
		choice = int(choice) - 1 	# subtract 1 from choice because the number
									#	entered by the user needs to match the
									#	index of the item[] and price[] lists
		price = item_price[choice]
	except ValueError:
		hum()
	return

def formatFloat(num):
	return format(num, '.2f')

# Allow the user to submit bills and coins to the vending machine
#  one at a time. There is a small chance for bills to be rejected.
def getCash():
	global price
	global reject_chance

	bill_type = 0
	cash 	= 0.0
	paid 	= 0.0
	cost = price
	

	# consider cost in terms of pennies
	cost *= 100
	
	while (cost > 0):
		global wallet
		clearScreen()

		# ask user to enter each denomination one at a time
		print(items[choice])
		print ("Please insert the following amount: ${}".format(formatFloat(cost/100)))
		print("Paid: ${}\tWallet: ${}".format(formatFloat(paid/100), formatFloat(wallet)))
		print ("1. $1.00\t2. $5.00")
		print ("3. $0.25\t4. $0.10")
		print ("5. $0.05\t6. $0.01")

		denom = input("> ")
		
		try:
			
			denom = float(denom)
			
			# if the denomination is a 1 or 5 dollar bill,
			#  there is a chance for the machine to reject the bill
			if (denom == 1):
				if (not rejectBill(reject_chance)):
					cash = 1.0
					bill_type = 1
				else:
					cash = 0

			elif(denom == 2):

				if (not rejectBill(reject_chance)):
					cash = 5.0
					bill_type = 5
				else:
					cash = 0

			elif(denom == 3):
				cash = 0.25

			elif(denom == 4):
				cash = 0.10

			elif(denom == 5):
				cash = 0.05

			elif(denom == 6):
				cash = 0.01

			else:
				hum()
			
			if (cash > wallet):
				print ("You do not have that much money")
			else:
				if (cash == 0):
					print ("The vending machine spits out the bill.")
					
					b = random.randint(0, 3)
					
					inspect = input("Inspect the bill? (Y/N)\n> ")
					
					inspect = inspect.upper()
					
					if (inspect == 'Y'):
						if (b == 2):
							print ("The bill is fine.")
							Wait()
						else:
							reject_chance = billEncounter(bill_type)
									
					
				cash *= 100
				paid = paid + cash
				wallet = ((wallet * 100) - cash) / 100
				
				cost -= cash
			
		except ValueError:
				hum()
			
	return paid / 100

""" Determine amount of each denomination. 
	If the user enters price p and cash c,
	denomination d is the difference int-divided by the
	denomination value.
"""
def calcChange(paid):
	global change_due
	global remaining
	global register
	remaining = 0
	
	difference = paid - item_price[choice]
	
	# multiply everything by 100 to make all calculations in terms of pennies
	difference *= 100
	register *= 100
	
	# quarters
	change_due[0] = difference // 25
	remaining = difference % 25
	register -= (25 * change_due[0])

	# dimes
	change_due[1] = remaining // 10
	remaining = remaining % 10
	register -= (10 * change_due[1])

	# nickels
	change_due[2] = remaining // 5
	remaining = remaining % 5
	register -= (5 * change_due[2])

	# pennies
	change_due[3] = remaining #// 1
	remaining = remaining % 1
	register -=  change_due[3]


	# cast denominations as integer to make output look nice
	for i in range(0, len(change_due)):
		change_due[i] = int(change_due[i])

	# divide the register by 100 to return it to unadjusted amount
	register /= 100
	#change_due = [twenties, tens, fives, ones, quarters, dimes, nickels, pennies]
	return

""" dispenseChange refers to the lists denominations[] and change_due[]
	in order to display the number of each denomination to be returned
	to the user
 """
def dispenseChange():
	global change_due
	global denominations
	global wallet
	total = 0.0

	clearScreen()
	print("Dispensing change:")

	# for each number in the range 0-7
	for i in range(0, len(change_due)):		# for each item in range 0-7

		# check that change_due > 0 so we only have to print
		#  the denominations we are actually returning to the user
		if (change_due[i] > 0):

			for coins in range(0, change_due[i]):
			
				print("*ka-chunk* {}".format(formatFloat(denominations[i]).rjust(5)))
				time.sleep(.5)

			# add up a running total of change returned to user
			total += denominations[i] * change_due[i]
	
	wallet += total
	print ("You received ${} in change".format(formatFloat(total)))
	print ("Your wallet contains ${}".format(formatFloat(wallet)))
	return

##  roundUp is designed to overcome some weird floating point shenanigans
##   in which a number is sometimes misrepresented after division.
##   For example 1.10 being represented as 1.09999999999999
##   roundUp accomplishes this by adding .005 to a number,
##   then chopping off anything beyond the hundredths place
##   and returning the result. 
def roundUp(num):
	return format((num + 0.005), '.2f')
	
# initialize vending machine stock and register
change_due = [None] * 4
denominations = [.25, .10, .05, .01]

choice = 0
items = ['Cola','Tea','Lemonade','Grape Juice','Water']

item_price = [1.75, 2.05, 0.50, 0.89, 1.15]
price = 0.0

register = 200
wallet = 10

reject_chance = 0

print ("Welcome to the vending machine simulator.")
print ("This machine accepts only $1 and $5 bills, and coins.")
Wait()

c = 'x'

while (c.upper() != 'Y'):
	printMenu()
	
	if (wallet > price):
		paid = getCash()
		if (paid > price):
			print("Amount paid greater than price: {} > {}".format(paid, price))
			calcChange(paid)
			dispenseChange()
	else:
		print ("You don't have enough money")
	c = input("Walk away? (Y/N)\n> ")

print ("You turn around and begin to walk away, the vending machine softly humming in the background.")

