#!/usr/bin/python

""" cardGame.py
    basic card game framework
    keeps track of card locations for as many hands as needed
"""
from random import *
seed()

NUMCARDS = 52
DECK = 0
PLAYER = 1
COMP = 2

# Store the *locations* of cards
#  where the index of cardLoc should
#  be the value of the card

cardLoc = [0] * NUMCARDS

suitName = ("hearts", "diamonds", "spades", "clubs")

rankName = ("Ace", "Two", "Three", "Four", "Five", "Six", "Seven", 
            "Eight", "Nine", "Ten", "Jack", "Queen", "King")

playerName = ("deck", "player", "computer")


## Initialize all cards to be stored in the deck
##  by setting all values in cardLoc to DECK
def clearDeck():
  for i in range(0, NUMCARDS):
    cardLoc[i] = DECK

"""
	Suit = index // 13
	rank = index % 13
"""  

def getSuit(index):

	suit = index // 13

	return suitName[suit]
	
def getRank(index):

	rank = index % 13
	
	return rankName[rank]

"""
Print a formatted list of all of the cards
  and their corresponding locations.

  ## Example output ##

  Location of all cards
  #        card        location
  0   Ace of hearts     deck
  1   Two of hearts     computer
  2   Three of hearts   player
  3   Four of hearts    deck

"""
def showDeck():

  print ("    Location of all cards")
  print ("#\tCard\t\tLocation")

  for i in range(0, NUMCARDS):
  
	suit_number = i // 13
	rank_number = i % 13
	
	suit = suitName[suit_number]
	rank = rankName[rank_number]
  
    print ("{} {} of {}\t{}".format(str(i).ljust(3), getRank(i), getSuit(i), playerName[cardLoc[i]] ))

  return

## Distribute a card to a specified participant;
##  either the human PLAYER or COMP
def assignCard(participant):

  # generate a random number to represent a card
  r = randint(0, NUMCARDS - 1)

  # check the card at location r to determine if the
  #  card exists in the deck.
  #  Draw cards until one that is not controlled by
  #  the PLAYER or COMP is drawn
  while cardLoc[r] != 0:
    r = randint(0, NUMCARDS - 1)

  # set the value of cardLoc at the generated
  #  memory location to participant
  cardLoc[r] = participant

  return

## Show the hand of the specified participant
def showHand(participant):

  participant_name = ""

  if participant == 1:
    participant_name = "Player"

  elif participant == 2:
    participant_name = "Computer"


  print ("\t{}\'s Hand".format(participant_name))

  for i in range(0, NUMCARDS):

    if cardLoc[i] == participant:
      print ("{} of {}".format(getRank(i), getSuit(i)))

  return

def main():
  clearDeck()

  for i in range(5):
    assignCard(PLAYER)
    assignCard(COMP)

  showDeck()
  showHand(PLAYER)
  showHand(COMP)  


if __name__ == "__main__":
  main()
