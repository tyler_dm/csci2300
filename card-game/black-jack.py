#!/usr/bin/python

"""
	Tyler Manifold
	CSCI230
	Card Game black belt challenge
	Black Jack
"""
import platform
import os

import random
random.seed()

NUMCARDS = 52
DECK = 0
PLAYER = 1
COMP = 2
GAME_TIE = 3

# Store the *locations* of cards
#  where the index of cardLoc should
#  be the value of the card

cardLoc = [0] * NUMCARDS

game_wins = [0, 0]

suitName = ("hearts", "diamonds", "spades", "clubs")

rankName = ("Ace", "Two", "Three", "Four", "Five", "Six", "Seven", 
			"Eight", "Nine", "Ten", "Jack", "Queen", "King")

playerName = ("deck", "player", "computer")


## Initialize all cards to be stored in the deck
##  by setting all values in cardLoc to DECK
def clearDeck():
	for i in range(0, NUMCARDS):
		cardLoc[i] = DECK

"""
	Suit = index // 13
	rank = index % 13
"""  

def getSuit(index):

	suit = index // 13

	return suitName[suit]
	
def getRank(index):

	rank = index % 13
	
	return rankName[rank]

"""
	Calculate sum of the card values in the specified participant's hand
	Loop through cardLoc
	Check if each card is belongs to participant
	test each card in hand against getRank to determine the numerical
"""
def calcHandTotal(participant):

	hand = [0]
	total = 0

	for i in range(NUMCARDS):

		if cardLoc[i] == participant:

			card_rank = getRank(i)
			hand.append(card_rank)

			#print ("cardLoc[{}]={}\trank={}".format(i, cardLoc[i], card_rank))

	for h in range(len(hand)):
	
		for n in range(len(rankName)):

			if hand[h] == rankName[n]:
				#print("hand[h]={}\trankName[n]={}\t n={}".format(hand[h], rankName[n], n))

				total += (n + 1)

	return total


"""
Print a formatted list of all of the cards
  and their corresponding locations.

  ## Example output ##

  Location of all cards
  #        card        location
  0   Ace of hearts     deck
  1   Two of hearts     computer
  2   Three of hearts   player
  3   Four of hearts    deck

"""
def showDeck():

	print ("    Location of all cards")
	print ("#\tCard\t\tLocation")

	for i in range(0, NUMCARDS):
		print ("{} {} of {}\t{}".format(str(i).ljust(3), getRank(i), getSuit(i), playerName[cardLoc[i]] ))


## Distribute a card to a specified participant;
##  either the human PLAYER or COMP
def assignCard(participant):

	# generate a random number to represent a card
	r = random.randint(0, NUMCARDS - 1)

	# check the card at location r to determine if the
	#  card exists in the deck.
	#  Draw cards until one that is not controlled by
	#  the PLAYER or COMP is drawn
	while cardLoc[r] != 0:
		r = random.randint(0, NUMCARDS - 1)

	# set the value of cardLoc at the generated
	#  memory location to participant
	cardLoc[r] = participant

	return

## Show the hand of the specified participant
def showHand(participant):

	participant_name = ""

	if participant == 1:
		participant_name = "Player"

	elif participant == 2:
		participant_name = "Dealer"

	print ("\n{}\'s Hand".format(participant_name))

	for i in range(0, NUMCARDS):

		if cardLoc[i] == participant:
			print ("  {} of {}".format(getRank(i), getSuit(i)))


	total = calcHandTotal(participant)

	print ("Total: {} | Wins: {}".format(total, game_wins[participant - 1]))

	return

def printMenu():

	print ("")
	print (" 1) Hit")
	print (" 2) Stay")
	print (" 0) Quit")

def initGame():

	clearDeck()

	assignCard(COMP)

	for i in range(2):
			assignCard(PLAYER)

def calcWinner():

	winner = GAME_TIE

	player_score = calcHandTotal(PLAYER)
	dealer_score = calcHandTotal(COMP)

	player_bust = False
	dealer_bust = False

	if player_score > 21:
		player_bust = True

	if dealer_score > 21:
		dealer_bust = True

	if player_bust and dealer_bust:
		winner = GAME_TIE

	else:
		if player_bust:
			winner = COMP
			game_wins[COMP - 1] += 1

		elif dealer_bust:
			winner = PLAYER
			game_wins[PLAYER - 1] += 1

		else:
			if player_score <= 21 and player_score > dealer_score:
				winner = PLAYER
				game_wins[PLAYER - 1] += 1

			elif dealer_score <= 21 and dealer_score > player_score:
				winner = COMP
				game_wins[COMP - 1] += 1

	return winner

def startHand():
	exit = False

	winner = 0

	initGame()

	hand_over = False

	while not exit:

		dealer_score = calcHandTotal(COMP)
		player_score = calcHandTotal(PLAYER)

		if player_score >= 21 or dealer_score > 17:
			hand_over = True

		while not hand_over:
			clearScreen()
			showHand(COMP)
			showHand(PLAYER)

			printMenu()

			choice = input ("\nWhat would you like to do?\n> ")
		
			if choice == "0":
				hand_over = True
				exit = True
			
			else:
				if choice == "1":
					assignCard(PLAYER)
					assignCard(COMP)

				elif choice == "2":
					assignCard(COMP)

				else:
					print ("Invalid input")

				dealer_score = calcHandTotal(COMP)
				player_score = calcHandTotal(PLAYER)

				if player_score >= 21 or dealer_score > 17:
					hand_over = True

		if not exit:
			clearScreen()

			winner = calcWinner()

			if winner == PLAYER:
				print ("Player Wins!")

			elif winner == COMP:
				print ("Dealer Wins!")

			elif winner == GAME_TIE:
				print ("Push!")

			else:
				print ("Could not determine a winner for this hand.")

			showHand(COMP)
			showHand(PLAYER)

			Wait()

			winner = 0
			initGame()
			hand_over = False

def main():

	exit = False

	wallet = 0

	showHand(COMP)
	showHand(PLAYER)

	startHand()

# Calls the appropriate clear screen function depending one
#  what system is running the program
def clearScreen():
	if (platform.system() == 'Windows'):
		a = os.system("cls")
	else:
		b = os.system("clear")

def Wait():
	input("Press ENTER to continue")

if __name__ == "__main__":
	main()
