#!/usr/bin/python

# Tyler Manifold
# CSCI230
# Knock, knock

# get user's name
name = input("Hi, what is your name?\n> ");

# say hello!
print("Hello, ", name, ".");

# begin knock knock joke
whos_there = input("Knock, knock.\n> ");

# get "X who?"
who = input("To.\n> ");

# print punchline
print("To *whom*");
