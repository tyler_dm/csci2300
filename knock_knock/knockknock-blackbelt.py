#!/usr/bin/python

# Tyler Manifold
# CSCI230
# Knock, knock blackbelt

import random

# init random seed
random.seed();

rand_limit = 4

jokes       = ['To', 'Orange', 'Ya', 'Europe'];
punchlines  = ['To *whom*', 'Orange you going to let me in?', 'No thanks, I prefer Google.', 'No, you\'re a poo!'];

def printJoke(num):

    # begin knock knock joke
    whos_there = input("Knock, knock.\n> ");

    # get "X who?"
    who = input(jokes[num] + "\n> ");

    # print punchline
    print(punchlines[num]);

    return;

# get user's name
name = input("Hi, what is your name?\n> ");

# say hello!
print("Hello, ", name, ".");
choice = input('1. Joke\n0. Exit\n> ');

# loop while user has not entered 0
while int(choice) > 0:
    x = random.randint(0, (rand_limit - 1)); # get a random int to pass to printJoke()
    printJoke(x);
    choice = input('1. Joke\n0. Exit\n> ');   

