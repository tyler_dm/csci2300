#!/usr/bin/python

# Tyler Manifold
# CSCI230
# High-Low Blackbelt
#  Try to guess a number thought of by the user

import random
random.seed()

# Set the guess ceiling to 100
GUESS_MAX = 100

# Set the guess floor to 1
GUESS_MIN = 0

correct = False
number_is_valid = True

tries = 1

guess = random.randint(GUESS_MIN, GUESS_MAX)

## Compare 'number' to the values of highest_value and lowest_value
## to determine if 'number' falls within the logical range
def isBetweenKnownValues(number):
	result = True

	if (not(number > GUESS_MIN and number < GUESS_MAX)):
		result = False
		#print ("new_guess: {} is not between {} and {}.".format(number, GUESS_MIN, GUESS_MAX))
	
	return result

## isValid is used to validate the user's input
##  by checking to see if they entered acceptable
##  input.
##  The only acceptable input for this program is
##  'C', 'H', and 'L'
def isValid(c):
	valid = True

	c = c.upper()

	if c == 'C':
		valid = True
	elif c == 'H':
		valid = True
	elif c == 'L':
		valid = True
	else:
		valid = False

	return valid

## genGuess generates a random integer between l_val
## and h_val and returns the result
def genGuess(l_val, h_val):

	new_guess = random.randint(l_val, h_val + 1)

	# Generate a new_guess until it within the acceptable range
	while not isBetweenKnownValues(new_guess):
		new_guess = genGuess(l_val, h_val)
	
	return new_guess

## If GUESS_MAX - 1 == GUESS_MIN + 1, then the mystery number
##  sits directly between the minimum and maximum. In this scenario
##  there is only one answer, so we return (known = True)
def mysteryNumKnown():
	known = False
	
	if (GUESS_MAX - GUESS_MIN) == 2:
		known = True
	
	return known
	
## Compare GUESS_MAX and GUESS_MIN to determine if the number the user
##  is thinking of has to be a decimal.
def guessIsDecimal():
	dec = False
	
	if (GUESS_MAX - 1) == GUESS_MIN:
		print ("You must be thinking of a decimal number between {} and {}".format(GUESS_MIN, GUESS_MAX))
		print ("I can only guess numbers from 1 to 100")
		dec = True
	
	return dec

print ("\nPlease think of a number from 1 to 100.")
print ("I will attempt to guess your number. Tell me")
print ("if I am too high, too low, or correct.")
	
while not correct and number_is_valid:
	# initially guess a random number

	print ("\nI know the number must be between {} and {}".format(GUESS_MIN, GUESS_MAX))
	print ("I guess: {}\n".format(guess))

	attempt = 'z'

	while not isValid(attempt):
		# ask user if the number is too high, too low, or correct
		attempt = input ("Is my guess too (h)igh, too (l)ow, or (c)orrect\n> ")
		
		
	attempt = attempt.upper()
	
	# If the guess is correct, set flag to exit loop
	if attempt == 'C':
		correct = True
		print ("I got it after {} tries!".format(tries))

	# If the guess is incorrect, ask the user if the number is
	#  too high or too low
	else:
		# If the number is too high
		if attempt == 'H':
		
			if guess < GUESS_MAX:
				GUESS_MAX = guess
				

			if guess <= 1:			
				print ("You are thinking of a number less than or equal to zero.")
				print ("I can only guess numbers from 1 to 100")
				number_is_valid = False

			elif guessIsDecimal():
				number_is_valid = False

			elif mysteryNumKnown():
				correct = True
				print ("Your number must be {}".format(GUESS_MAX - 1))
				print ("I got it after {} tries!".format(tries))

			else:
				guess = genGuess(GUESS_MIN, GUESS_MAX)

		# if the number is too low
		if attempt == 'L':
		
			if guess > GUESS_MIN:
				GUESS_MIN = guess
		
			if guess >= 100:
				print ("You are thinking of a number greater than or equal to 100.")
				print ("I can only guess numbers from 1 to 100")
				number_is_valid = False
				
			elif guessIsDecimal():
				number_is_valid = False
			
			elif mysteryNumKnown():
				correct = True
				print ("Your number must be {}".format(GUESS_MAX - 1))
				print ("I got it after {} tries!".format(tries))
				
			else:			
				guess = genGuess(GUESS_MIN, GUESS_MAX)

		tries += 1
		