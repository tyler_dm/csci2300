
#!/usr/bin/python

# Tyler Manifold
# CSCI230
# High Low
#  Ask the user to guess a number
#  Tell the user if the number is too High
#  or too low.

import random

random.seed()

# Generate a random number between 1 and 100
the_number = random.randint(1, 101)

incorrect = True
attempt = 1

print ("I am thinking of an integer between 1 and 100.")
print ("See if you can guess the number. I will tell you if")
print ("your guess is too high or too low.")

# ask the user to guess the number until they are correct
while incorrect:
	guess = input("{}) > ".format(attempt))
	guess = int(guess)

	# Exit the loop if the guess is correct
	if guess == the_number:
		print ("{} is correct!".format(guess))
		print ("You guessed the number after {} attempt(s)!".format(attempt))
		incorrect = False

	# otherwise test to see if the guess is too high or too low
	else:
		# tell the user if the guess is too high
		if guess > the_number:
			print ("Too high!")
		# tell the user if the guess is too low
		else:
			print ("Too low!")

		# keep track of the number of attempts
		attempt += 1


