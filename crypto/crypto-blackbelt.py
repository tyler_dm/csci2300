#!/usr/bin/python
""" crypto-blackbelt.py
    Implements a simple substitution cypher

    Tyler Manifold
    CSC230
"""

import random
import platform
import os

random.seed()

KEY_SIZE = 25

alpha = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890!@#$%^&*<>?"
key =   ""

show_key = False

def main():

  global show_key
  global key

  key = genKey()

  keepGoing = True
  
  while keepGoing:
  
    response = menu()
  
    if response == "1":
  
      plain = input("text to be encoded: ")
  
      print(encode(plain, key))

      Wait()
  
    elif response == "2":
  
      coded = input("code to be decyphered: ")
  
      print (decode(coded, key))

      Wait()

    elif response == "3":

      key = genKey()

    elif response == "4":

        show_key = not show_key

    elif response == "5":

        s = input("\nEnter the new random seed:\n> ")

        random.seed(s)
        key = genKey()
  
    elif response == "0":
  
      print ("Thanks for doing secret spy stuff with me.")
  
      keepGoing = False
  
    else:
      print ("I don't know what you want to do...")

def menu():

  global show_key
  global key

  clearScreen()

  print ("\nSECRET DECODER MENU\n")
  if show_key:
    print ("\nKey: " + key + "\n")

  print ("0) Quit")
  print ("1) Encode")
  print ("2) Decode")
  print ("3) Generate New Key")

  if show_key:
    print ("4) Hide Key")
  else:
    print ("4) Show Key")

  print ("5) Set random seed")

  choice = input ("What do you want to do?\n> ")

  return choice

def encode(str, key):
  
  str_encoded = ""

  # Generate a rand

  # swap the letters in str with the corresponding letters in key
  for letter in range(0, len(str)):

    for i in range(0, len(alpha)):

      if str[letter].upper() == alpha[i]:
        str_encoded += key[i]

  return str_encoded

def decode(str, key):
  
  str_decoded = ""

  # swap the letters in key with the corresponding letters in alpha

  for letter in range(0, len(str)):

    for i in range(0, len(key)):

      if str[letter].upper() == key[i]:
        str_decoded += alpha[i]

  return str_decoded

def genKey():

  print ("\nGenerating a new key...\n")

  # initialize the new key with a letter
  new_letter = genLetter()
  new_key = new_letter
  key_length = 0

  #print ("new_key initialized to " + new_key + ". new_key length: {}".format(len(new_key)))

  # while the number of letters we have is less than the alpha
  #  generate a new random letter and append it to the key
  keepGoing = True
  while keepGoing:

    # Generate a new random letter until the letter generated
    #  is not already found in the new_key
    while isDuplicate(new_letter, new_key):
      new_letter = genLetter()

    new_key += new_letter
    key_length += 1

    #print ("new_key: " + new_key)
    #print ("key_length: {}".format(len(new_key)))

    if key_length >= KEY_SIZE:
      keepGoing = False
    
  return new_key

## Generate a random number and return the value
##  at the corresponding index of alpha
def genLetter():
  r = random.randint(0, len(alpha) - 1)
  return alpha[r]

## Iterate through test_list to test for occurrance
##  of letter
def isDuplicate(letter, test_list):
  dupe = False

  for i in range(0, len(test_list)):
    if letter == test_list[i]:
      dupe = True

  return dupe

# Calls the appropriate clear screen function depending one
#  what system is running the program
def clearScreen():
  if (platform.system() == 'Windows'):
    a = os.system("cls")
  else:
    b = os.system("clear")

def Wait():
  input("Press ENTER to continue")
  return
## END OF DEFINITIONS ##
if __name__ == "__main__":
    main()

