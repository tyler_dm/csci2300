#!/usr/bin/python
""" crypto.py
    Implements a simple substitution cypher

    Tyler Manifold
    CSC230
"""

alpha = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
key =   "XPMGTDHLYONZBWEARKJUFSCIQV"

def main():

  keepGoing = True
  
  while keepGoing:
  
    response = menu()
  
    if response == "1":
  
      plain = input("text to be encoded: ")
  
      print(encode(plain))
  
    elif response == "2":
  
      coded = input("code to be decyphered: ")
  
      print (decode(coded))
  
    elif response == "0":
  
      print ("Thanks for doing secret spy stuff with me.")
  
      keepGoing = False
  
    else:
      print ("I don't know what you want to do...")

def menu():

  print ("SECRET DECODER MENU\n")
  print ("0) Quit")
  print ("1) Encode")
  print ("2) Decode\n")

  choice = input ("What do you want to do?\n> ")

  return choice

def encode(str):
  
  str_encoded = ""

  # swap the letters in str with the corresponding letters in key
  for letter in range(0, len(str)):

    for i in range(0, len(alpha)):

      if str[letter].upper() == alpha[i]:
        str_encoded += key[i]

  return str_encoded

def decode(str):
  
  str_decoded = ""

  # swap the letters in key with the corresponding letters in alpha

  for letter in range(0, len(str)):

    for i in range(0, len(alpha)):

      if str[letter].upper() == key[i]:
        str_decoded += alpha[i]

  return str_decoded

## END OF DEFINITIONS ##
if __name__ == "__main__":
    main()
